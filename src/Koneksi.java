
import java.awt.List;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Rafishalahudin
 */
public class Koneksi {
    static Connection koneksi;
    
     public static Connection getKoneksi(String host, String port, 
	String username, String password, String db) {
        String konString = "jdbc:mysql://" + host + ":" + port + 
		"/" + db;
        Connection koneksi = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            koneksi = DriverManager.getConnection(konString, 
		     username, password);
            System.out.println("Koneksi Berhasil");
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Koneksi Database Error");
            koneksi = null;
        }
        return koneksi;
    }
   
     
     
//     public static ArrayList<String> getAllSocketName(){
//         String query = "SELECT jenis_socket FROM t_socket";
//         Statement stmt = koneksi.createStatement();
//         ResultSet rs = stmt.executeQuery(query);
//         ArrayList<String> asd = new ArrayList<String>(); 
//         while(rs.next()){
//             asd.add(rs.getString("jenis_socket"));
//         }
//     }
}
