
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Rafishalahudin
 */
public class Getter {

    Connection koneksi = Koneksi.getKoneksi("localhost", "3306", "root", "", "db_simulasi");

    public String getIdProc(String proc) {

        try {
            Statement stmt = koneksi.createStatement();
            String query = "SELECT * FROM t_processor WHERE nama_processor='" + proc + "'";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                return rs.getString("id_processor");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, " nama processorr Terjadi kesalahan pada database");

        }
        return "Kosong";
    }

    public String getIdVga(String vga) {
        try {
            Statement stmt = koneksi.createStatement();
            String query = "SELECT * FROM t_vga WHERE nama_vga='" + vga + "'";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                return rs.getString("id_vga");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, " nama vgaa Terjadi kesalahan pada database");

        }
        return "Kosong";
    }

    public String getIdRam(String ram) {
        try {
            Statement stmt = koneksi.createStatement();
            String query = "SELECT * FROM t_ram WHERE nama_ram='" + ram + "'";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                return rs.getString("id_ram");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, " nama ramm Terjadi kesalahan pada database");

        }
        return "Kosong";
    }

    public String getIdPsu(String psu) {
        try {
            Statement stmt = koneksi.createStatement();
            String query = "SELECT * FROM t_powersuply WHERE nama_powersuply='" + psu + "'";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                return rs.getString("id_powersuply");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, " nama psuu Terjadi kesalahan pada database");
        }
        return "Kosong";
    }

    public String getIdHdd(String hdd) {
        try {
            Statement stmt = koneksi.createStatement();
            String query = "SELECT * FROM `db_simulasi`.`t_harddisk` WHERE `nama_harddisk`='" + hdd + "';";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                return rs.getString("id_harddisk");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, " nama hddd Terjadi kesalahan pada database");
        }
        return "Kosong";
    }

    public String getIdMobo(String mobo) {
        try {
            Statement stmt = koneksi.createStatement();
            String query = "SELECT * FROM `db_simulasi`.`t_socket` WHERE `jenis_socket`='" + mobo + "';";
            ResultSet rs = stmt.executeQuery(query);
            while (rs.next()) {
                return rs.getString("id_socket");
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, " nama mobooo Terjadi kesalahan pada database");

        }
        return "Kosong";
    }

    public void SimpanFilekeDb(String dir, String file) {
        String Extension = file.substring(file.lastIndexOf("."), file.length());
        dir = dir.replace("\\", "\\\\");
    }
}
